
//Vitalii Kapitan, Egor Vasiliev

#define WIDTH 64					//Ширина i
#define HEIGHT 16					//Высота j
#define LENGTH 1					// Кол-во слоев k  (DEPTH)
#define N (WIDTH * HEIGHT * LENGTH) // Размер системы
#define STEP 1e6				// Монте-Карло шаг 
#define ELT STEP * 1.0				// Каждый i-й элт, для графика на i-ом элт-е МК-шага (визуализация STEP * 1.0 = последний мк шаг)
#define CYCLE 100 //1					// К-во циклов программ

#define LOOP 1 // Система бесконечна (1) или с границами (0)

// Комментарий для визуализации
#define KOMENT "Версия LITE 0.6 \n\
- Граничные условия\n\
- Соседа сверху и снизу (k) нет В ЛЮБОМ СЛУЧАЕ\n\
- Градиент поля H_xy \n\
"

#define B_N "B_new" //Bloch - 'B' Neel - 'N' // B_new
//Циклы
#define Jex 1.0
#define maxJ 1.0
#define stepJ 1.0

#define DMI 0.5 // DM вектор (=1.5)
#define maxD 0.5
#define stepD 1.0

#define POLE 0.25 //  Внешнее поле < DMI (=1)
#define maxP 0.25
#define stepP 1.0

#define An 0.0 //  A
#define maxA 0.0
#define stepA 1.0

#define minT 1	//50  // Минимальная температура       *  max - min
#define maxT 2	//2050 // Максимальная температура     *  --------- = mainN
#define stepT 1 //50 // Шаг температура                *    step

double STT = 0.0;

#define _T 1000. // 1000. число, на которое делится Т 000000000.

#define _USE_MATH_DEFINES //#define M_PI 3.14159265358979

#include <iostream>
#include <cmath>
#include <iomanip>
//#include <windows.h>
//#include <time.h>
//#include <cstdio>
//#include <cstdlib>
#include <fstream>
#include <sstream>
//#include <string>
#include <cstring>

//#include <bitset>
//#include <direct.h>
#include <sys/stat.h>
//#include <sys/types.h>
#include <random>
#include <conio.h> // _getch
//#include <algorithm>


#define watch(x) cout << (#x) << " is " << (x) << endl // watch(переменная) выведет имя переменной и её значение

using namespace std;

#include <ctime>

mt19937 gen(time(0));
uniform_real_distribution<double> urd(0., 1.);
uniform_real_distribution<double> phi_rand(0., 2. * M_PI);
uniform_real_distribution<double> teta_rand(0., M_PI);

struct SpinStruct
{
	double X, Y, Z; //{0};

	int U, R, D, L, F, B; //UP, RIGHT, DOWN, LEFT, FRONT, BACK;

	double Anisotropy;

	double Efield_X{0};
	double Efield_Z{0};
};
SpinStruct spin[N + 1];

// O(3) Нелинейная сигма-модель (Non-linear sigma model)
void sigma_model(SpinStruct *spin, float i_x = 0, float j_y = 0, double h = 1., bool fill = false, int r = 1, string BN = B_N)
{
	/*
		i_x, j_y - координаты центра
		h - размер ядра скирмиона
		fill - обрезать скирмион 0, или заполнить всю систему 1
		r - направление. Для B "1" - по часовой, "-1" - против. Для N "1" - от центра, "-1" - к центру
	*/
	double p = 1.;
	for (int i = 0; i < WIDTH; ++i)
	{
		for (int j = 0; j < HEIGHT; ++j)
		{
			float x = (i - i_x); // правильный центр координат
			if (x >= WIDTH / 2.)
				x -= WIDTH; // повтор системы (закольцованность относительно длины и ширины)
			else if (x < -WIDTH / 2.)
				x += WIDTH;
			float y = (j - j_y);
			if (y >= HEIGHT / 2.)
				y -= HEIGHT;
			else if (y < -HEIGHT / 2.)
				y += HEIGHT;

			if (Jex < 0)
				p = pow((-1), (x + y)); // AFM pow(-1); FM pow(1)

			double d = (x * x + y * y + h * h);
			if (d == 0) // if костыль для условия h=0
			{
				d = 1.;
				x = 1.;
			};

			//if(sqrt(x*x + y*y) <= h*h)
			if (fill || (x * x + y * y <= (h * h * M_PI))) //(h^4) 2pi  (h * h * M_PI + h)
			{
				if ((BN == "B") || (BN == "B_new"))
				{
					spin[i * HEIGHT + j].X = (-1.) * r * p * (h * y / d);
					spin[i * HEIGHT + j].Y = r * p * (h * x / d);
				}
				else if (BN == "N")
				{
					spin[i * HEIGHT + j].X = r * p * (h * x / d);
					spin[i * HEIGHT + j].Y = r * p * (h * y / d);
				}
				else if (BN == "A")
				{
					spin[i * HEIGHT + j].X = r * p * (h * y / d);
					spin[i * HEIGHT + j].Y = r * p * (h * x / d);
				}
				else
				{
					cout << "Unknown type " << BN << endl;
					exit(707);
				}

				spin[i * HEIGHT + j].Z = p * ((x * x + y * y - h * h) / d);
				//cout<<"\t x "<<x<<"   y "<<y<<"\t"<<(((x * x + y * y - h * h) / d))<<endl;
			}
			//			else
			//			{
			//				spin[i * HEIGHT + j].X = 0;
			//				spin[i * HEIGHT + j].Y = 0;
			//				spin[i * HEIGHT + j].Z = p;
			//
			//			}
		}
	}
}

void Spin_sys(SpinStruct *spin, double phi = M_PI, double theta = M_PI / 2., int width = WIDTH, int height = HEIGHT, int length = LENGTH)
{
	int p = 1;
	for (int k = 0; k < length; ++k)
	{
		for (int i = 0; i < width; ++i)
		{
			for (int j = 0; j < height; ++j)
			{
				if (Jex < 0)
					p = ((i + j) % 2 == 0) ? -1 : 1; // разворачиваем Z для антифрро  // pow((-1), (i + j)); // (i+j) & 1; //

				spin[k * width * height + i * height + j].X = p * sin(theta) * cos(phi);
				spin[k * width * height + i * height + j].Y = p * sin(theta) * sin(phi);
				spin[k * width * height + i * height + j].Z = p * cos(theta);
			}
		}
	}
	// Граничный спин
	spin[N].X = 0.;
	spin[N].Y = 0.;
	spin[N].Z = 0.;
}

// Доменная стенка (одна полоса)
void domain_wall(SpinStruct *spin, double angle = 0., double b = 0.5, float i_x = WIDTH * 0.5, float j_y = HEIGHT * 0.5, int width = WIDTH, int height = HEIGHT, int length = LENGTH)
{
	/*
		i_x, j_y - координаты центра (вокруг этой точки происходит вращение)
		b - толщина стенки (0; 1] (чем меньше, тем больше стенка)
		angle - угол поворота в градусах
	*/
	int p = 1;
	angle = angle * M_PI / 180.;
	double phi = -angle + M_PI_2; // (BN == 'B') ? M_PI_2 : 0; //

	for (int k = 0; k < length; ++k)
	{
		for (int i = 0; i < width; ++i)
		{
			for (int j = 0; j < height; ++j)
			{
				double x = (double)((i - i_x) * cos(angle) - (j - j_y) * sin(angle)); // Поворот по часовой

				double theta = 4 * atan(exp((x)*b)); //4*atan(exp((i-10)*0.25)); // создание доменной стенки

				if (Jex < 0)
					p = ((i + j) % 2 == 0) ? -1 : 1; // разворачиваем Z для антифрро

				spin[k * width * height + i * height + j].X = -1 * p * sin(theta) * cos(phi);
				spin[k * width * height + i * height + j].Y = -1 * p * sin(theta) * sin(phi);
				spin[k * width * height + i * height + j].Z = p * cos(theta);
			}
		}
	}
}

void spin_wave(SpinStruct *spin, double angle = 0., double b = 0.5, float i_x = 0, float j_y = 0, int width = WIDTH, int height = HEIGHT, int length = LENGTH)
{
	/*
		i_x, j_y - координаты центра (вокруг этой точки происходит вращение)
		b - толщина стенки (0; 1] (чем меньше, тем больше стенка)
		angle - угол поворота в градусах
	*/
	angle = angle * M_PI / 180.;
	double phi = -angle + M_PI_2;

	for (int k = 0; k < length; ++k)
	{
		for (int i = 0; i < width; ++i)
		{
			for (int j = 0; j < height; ++j)
			{
				double x = (double)((i + i_x) * cos(angle) - (j + j_y) * sin(angle));
				double theta = (x)*b;

				spin[k * width * height + i * height + j].X = -1. * sin(theta) * cos(phi); //sin(theta) * cos(phi);//cos(i/2.) - Неель
				spin[k * width * height + i * height + j].Y = -1. * sin(theta) * sin(phi); //sin(theta)*sin(phi); //- Блох
				spin[k * width * height + i * height + j].Z = cos(theta);
			}
		}
	}
}

int nbr(int i, int j, int k, int width = WIDTH, int height = HEIGHT, int length = LENGTH)
{
	//формула куба, т.е. принадлежит ли точка кубу
	return (k * width * height + i * height + j);
}

int unique_nbr(int i, int j, int k, int width = WIDTH, int height = HEIGHT, int length = LENGTH)
{
	if (LOOP)
	{
		if (j >= height)
			return (k * width * height + i * height); // UP j=0
		else if (i >= width)
			return (k * width * height + j); // RIGHT i=0
		else if (j < 0)
			return (k * width * height + i * height + (height - 1)); // DOWN j=(height-1)
		else if (i < 0)
			return (k * width * height + (width - 1) * height + j); // LEFT i=(width-1)
		// Сверху и снизу нет соседа
		else if (k >= length)
			return N; // FRONT
		else if (k < 0)
			return N; // BACK
		else
			exit(185);
	}
	else
	{
		return N;
	}
}
//нахождение соседей
void neighbor(SpinStruct *spin, int width = WIDTH, int height = HEIGHT, int length = LENGTH)
{
	for (int k = 0; k < length; ++k)
	{
		int K = k * width * height;
		for (int i = 0; i < width; ++i)
		{
			int I = i * height;
			for (int j = 0; j < height; ++j)
			{
				spin[K + I + j].U = (j + 1 >= height) ? unique_nbr(i, j + 1, k) : K + I + (j + 1);		   // UP neighbor's number
				spin[K + I + j].R = (i + 1 >= width) ? unique_nbr(i + 1, j, k) : K + (i + 1) * height + j; // RIGHT neighbor's number
				spin[K + I + j].D = (j - 1 < 0) ? unique_nbr(i, j - 1, k) : K + I + (j - 1);			   // DOWN neighbor's number
				spin[K + I + j].L = (i - 1 < 0) ? unique_nbr(i - 1, j, k) : K + (i - 1) * height + j;	   // LEFT neighbor's number
				// 3D + back and forth
				spin[K + I + j].F = (k + 1 >= length) ? unique_nbr(i, j, k + 1) : (k + 1) * width * height + I + j; // FRONT neighbor's number
				spin[K + I + j].B = (k - 1 < 0) ? unique_nbr(i, j, k - 1) : (k - 1) * width * height + I + j;		// BACK neighbor's number
			}
		}
	}
}
//Анизотропия
void Anis(SpinStruct *spin, double A = An, int width = WIDTH, int height = HEIGHT)
{

	for (int i = 0; i < width; ++i)
	{
		for (int j = 0; j < height; ++j)
		{
			spin[i * height + j].Anisotropy = A;
			//			float val = -1;
			//			do
			//			{
			//				val = urd(gen); //rand[0,1]
			//			}
			//			while (val < 0.4f || val > 0.7f);
			//			spin[i * height + j].Anisotropy = val;		
		}
	}
}
void Anis_grad(SpinStruct *spin, double Astart = An, double Astop = 0., int part = WIDTH, int width = WIDTH, int height = HEIGHT)
{
	
	/* 
		Плавное изменение анизотропии от Astart до Astop (слева направо)
		part - больше 1 и меньше ширины "плавность" изменения градиента (количесто лесенок, т.е на сколько будет поделена ширина) 
	*/
	double Anis = Astart, step = 0;
	int sign = Astart > Astop ? -1 : +1;

	step = fabs(Astart-Astop) *  1./(part-1.);//double(double(width)/(part))/double(width);

	for (int i = 0; i < width; ++i)
	{	
		for (int j = 0; j < height; ++j)
		{
			spin[i * height + j].Anisotropy = Anis;
		}
		
		if((i+1) % (width/part) == 0) // каждую part часть плавно меняем анизотропию
			Anis += sign* step;
	}
}

// Энеогия одного спина
double Esp(SpinStruct *spin, int num, double J = Jex, double P = POLE, double DM = DMI, double A = An, double Px = 0.0, double Py = 0.0)
{
	double E_x = spin[num].X * (spin[spin[num].U].X + spin[spin[num].R].X + spin[spin[num].D].X + spin[spin[num].L].X + spin[spin[num].F].X + spin[spin[num].B].X);
	double E_y = spin[num].Y * (spin[spin[num].U].Y + spin[spin[num].R].Y + spin[spin[num].D].Y + spin[spin[num].L].Y + spin[spin[num].F].Y + spin[spin[num].B].Y);
	double E_z = spin[num].Z * (spin[spin[num].U].Z + spin[spin[num].R].Z + spin[spin[num].D].Z + spin[spin[num].L].Z + spin[spin[num].F].Z + spin[spin[num].B].Z);

	double E_u = 0., E_r = 0., E_d = 0., E_l = 0., E_f = 0., E_b = 0.;

	double DMI_x = 0., DMI_y = 0., DMI_z = 0.;
	if (B_N == "B")
	{
		// Отн-но координат В ПРАВОМ ортонормированном базисе
		E_u = spin[num].Z * spin[spin[num].U].X - spin[num].X * spin[spin[num].U].Z; //+y
		E_r = spin[num].Y * spin[spin[num].R].Z - spin[num].Z * spin[spin[num].R].Y; //+x
		E_d = spin[num].X * spin[spin[num].D].Z - spin[num].Z * spin[spin[num].D].X; //-y
		E_l = spin[num].Z * spin[spin[num].L].Y - spin[num].Y * spin[spin[num].L].Z; //-x

		E_f = spin[num].X * spin[spin[num].F].Y - spin[num].Y * spin[spin[num].F].X; //+z
		E_b = spin[num].Y * spin[spin[num].B].X - spin[num].X * spin[spin[num].B].Y; //-z
		DMI_x = (E_u + E_r + E_d + E_l + E_f + E_b);
	}
	else if (B_N == "N")
	{
		E_u = spin[num].Z * spin[spin[num].U].Y - spin[num].Y * spin[spin[num].U].Z; //+x
		E_r = spin[num].Z * spin[spin[num].R].X - spin[num].X * spin[spin[num].R].Z; //-y
		E_d = spin[num].Y * spin[spin[num].D].Z - spin[num].Z * spin[spin[num].D].Y; //-x
		E_l = spin[num].X * spin[spin[num].L].Z - spin[num].Z * spin[spin[num].L].X; //+y
		/*
		 spin[num].X * spin[spin[num].L].Z - spin[num].Z * spin[spin[num].L].X + spin[num].Z * spin[spin[num].R].X - spin[num].X * spin[spin[num].R].Z

		*/
	}
	else if(B_N == "B_new"){
		DMI_x = spin[num].X * (spin[spin[num].D].Z - spin[spin[num].U].Z);
        DMI_y = spin[num].Y * (spin[spin[num].R].Z - spin[spin[num].L].Z);
        DMI_z = spin[num].Z * (spin[spin[num].U].X - spin[spin[num].R].Y - spin[spin[num].D].X + spin[spin[num].L].Y);
	}
	/*________________________________ПОЛЕ-АНИЗОТРОПИЯ-ОБМЕННЫЙ ИНТЕГРАЛ________________________________*/
	double Pole_Z = P  * spin[num].Z;
	double Pole_X = Px * spin[num].X;
	double Pole_Y = Py * spin[num].Y;
	
	// Spin Transfer Torque ------------------------------------------------
	double	st_u, st_r, st_d, st_l;
	{//==============все соседи смотрят -x (лево)
//			st_u = spin[num].Z * spin[spin[num].U].X - spin[num].X * spin[spin[num].U].Z; //+y
//			st_r = spin[num].Z * spin[spin[num].R].X - spin[num].X * spin[spin[num].R].Z; 
//			st_d = spin[num].Z * spin[spin[num].D].X - spin[num].X * spin[spin[num].D].Z; 
//			st_l = spin[num].Z * spin[spin[num].L].X - spin[num].X * spin[spin[num].L].Z;
	}
	{//==============все соседи смотрят +x (право)
//		st_u = spin[num].X * spin[spin[num].U].Z - spin[num].Z * spin[spin[num].U].X; //+y
//		st_r = spin[num].X * spin[spin[num].R].Z - spin[num].Z * spin[spin[num].R].X; 
//		st_d = spin[num].X * spin[spin[num].D].Z - spin[num].Z * spin[spin[num].D].X; 
//		st_l = spin[num].X * spin[spin[num].L].Z - spin[num].Z * spin[spin[num].L].X;
	}
	{//==============все соседи смотрят +y (верх)
		// st_u = spin[num].Y * spin[spin[num].U].Z - spin[num].Z * spin[spin[num].U].Y; //+y
		// st_r = spin[num].Y * spin[spin[num].R].Z - spin[num].Z * spin[spin[num].R].Y; 
		// st_d = spin[num].Y * spin[spin[num].D].Z - spin[num].Z * spin[spin[num].D].Y; 
		// st_l = spin[num].Y * spin[spin[num].L].Z - spin[num].Z * spin[spin[num].L].Y;
	}
	{//==============все соседи смотрят -y (низ)
//		st_u = spin[num].Z * spin[spin[num].U].Y - spin[num].Y * spin[spin[num].U].Z; //+y
//		st_r = spin[num].Z * spin[spin[num].R].Y - spin[num].Y * spin[spin[num].R].Z; 
//		st_d = spin[num].Z * spin[spin[num].D].Y - spin[num].Y * spin[spin[num].D].Z; 
//		st_l = spin[num].Z * spin[spin[num].L].Y - spin[num].Y * spin[spin[num].L].Z;
	}
	{//==============все соседи смотрят ? от тебя
//		st_u = spin[num].X * spin[spin[num].U].Y - spin[num].Y * spin[spin[num].U].X; //+y
//		st_r = spin[num].X * spin[spin[num].R].Y - spin[num].Y * spin[spin[num].R].X; 
//		st_d = spin[num].X * spin[spin[num].D].Y - spin[num].Y * spin[spin[num].D].X; 
//		st_l = spin[num].X * spin[spin[num].L].Y - spin[num].Y * spin[spin[num].L].X;
	}
	{//==============все соседи смотрят ? на тебя
//		st_u = spin[num].Y * spin[spin[num].U].X - spin[num].X * spin[spin[num].U].Y; //+y
//		st_r = spin[num].Y * spin[spin[num].R].X - spin[num].X * spin[spin[num].R].Y; 
//		st_d = spin[num].Y * spin[spin[num].D].X - spin[num].X * spin[spin[num].D].Y; 
//		st_l = spin[num].Y * spin[spin[num].L].X - spin[num].X * spin[spin[num].L].Y;
	}
	 
	double sst_x = 0.0;//STT*(st_u + st_r + st_d + st_l);

	//Anis
	double Anis = spin[num].Anisotropy * spin[num].Z * fabs(spin[num].Z) ; // A*Sz^2 (с сохранением знака)
	//	double Anis = spin[num].Anisotropy * sin(acos(spin[num].Z))*sin(acos(spin[num].Z));
	/*____________________________________________________________________*/ 

	double E = (-1.) * J * (E_x + E_y + E_z) - /* DMI */ DM * (DMI_x + DMI_y + DMI_z) - /*Anisotropy*/ Anis - /*Magnetic field*/ Pole_Z - Pole_X - Pole_Y - sst_x;

	return E;
}

double Efull(SpinStruct *spin, double J = Jex, double P = POLE, double DM = DMI, double A = An, double Px = 0., double Py = 0.)
{
	double E = 0.;
	for (int num = 0; num < N; ++num)
	{
		E += Esp(spin, num, J, P, DM, A, Px, Py);
	}
	E /= (double)N;
	return 0.5 * E;
}

double Hex(SpinStruct *spin){
	double E_x=0.,E_y=0.,E_z=0.;
	for (int num = 0; num < N; ++num)
	{
	 E_x += spin[num].X * (spin[spin[num].U].X + spin[spin[num].R].X + spin[spin[num].D].X + spin[spin[num].L].X + spin[spin[num].F].X + spin[spin[num].B].X);
	 E_y += spin[num].Y * (spin[spin[num].U].Y + spin[spin[num].R].Y + spin[spin[num].D].Y + spin[spin[num].L].Y + spin[spin[num].F].Y + spin[spin[num].B].Y);
	 E_z += spin[num].Z * (spin[spin[num].U].Z + spin[spin[num].R].Z + spin[spin[num].D].Z + spin[spin[num].L].Z + spin[spin[num].F].Z + spin[spin[num].B].Z);
	}
	double E = (-1.) * Jex * (E_x + E_y + E_z) / (2.*N);
	return E;
}


void monte_carlo(SpinStruct *spin, int num, double T, double J = Jex, double P = POLE, double DM = DMI, double A = An, double Px = 0., double Py = 0.)
{
	double Old_spin_x = spin[num].X; // Сохраняем исходную конфигурацию
	double Old_spin_y = spin[num].Y;
	double Old_spin_z = spin[num].Z;

	double E1 = Esp(spin, num, J, P, DM, A, Px, Py); // энергия исходной конфигурации СПИНА

	//Спин МЕНЯЕТСЯ

	double phi = (2. * M_PI * (double)rand() / RAND_MAX);		//phi_rand(gen);	//2. * M_PI * urd(gen);		   //
	double theta = acos((2. * (double)rand() / RAND_MAX) - 1.); //teta_rand(gen); //acos((2. * urd(gen) - 1.)); // M_PI * (double)rand() / (double)RAND_MAX; //
	spin[num].X = sin(theta) * cos(phi);
	spin[num].Y = sin(theta) * sin(phi);
	spin[num].Z = cos(theta);

	double E2 = Esp(spin, num, J, P, DM, A, Px, Py); //энергия «новой» конфигурации

	if (E2 > E1)
	{
		//	double p = exp((E1 - E2) / (T));
		//	double s = (double)(rand()) / RAND_MAX; // urd(gen); //

		//	if (p < s)
		if (exp((E1 - E2) / T) < (double)rand() / RAND_MAX) //urd(gen)
		{													// спин остается в исходном состоянии
			spin[num].X = Old_spin_x;						// Сохраняем исходную конфигурацию
			spin[num].Y = Old_spin_y;
			spin[num].Z = Old_spin_z;
		}
	}
}

void ground_state(SpinStruct *spin, bool rnd = 0, bool hot = true, double J = Jex, double D = DMI, double P = POLE, double A = An, int n = N)
{

	double phi, theta;
	if (rnd)
	{
		for (int i = 0; i < n; ++i)
		{
			//phi = phi_rand(gen);
			//theta = teta_rand(gen);
			phi = (2. * M_PI * (double)rand() / RAND_MAX);
			theta = acos(1. - (2. * (double)rand() / RAND_MAX));

			(spin+i)->X = sin(theta) * cos(phi);
			(*(spin+i)).Y = sin(theta) * sin(phi);
			spin[i].Z = cos(theta);
		}
	}
	else
	{
		Spin_sys(spin, 1., 0.);
		sigma_model(spin, WIDTH/2., HEIGHT/2, 2);

	}

	if (hot)
	{
		for (double i = 1.; i <= 1000.; ++i) // разогреваем
			monte_carlo(spin, rand() % n, i / 100., J, P, D, A);

		for (double i = 1000.; i > 0.; --i) // охлаждаем
			monte_carlo(spin, rand() % n, i / 100., J, P, D, A);
	}

	//*/
}

void DATA() // Информация о начальной конфигурации + комментарий
{

	ofstream Information("conf/(DATA)");
	Information << KOMENT << "\n WIDTH \t" << WIDTH << "\n HEIGHT \t" << HEIGHT << "\n LENGTH \t" << LENGTH << "\n N \t" << N
				<< "\n STEP \t" << STEP << "\n ELT \t" << ELT << "\n POLE \t" << POLE << "\n DMI \t" << DMI << "\n B_N \t" << B_N << endl;
	Information.close();
}

void config(int Name, double dirJ, double dirD, double dirP, double dirA, double dirT, double *H_xy, SpinStruct *spin, string filename = "sys", int width = WIDTH, int height = HEIGHT, int length = LENGTH)
{
	/*// создание директории DMI
	ostringstream convertDMI;
	convertDMI << dirD;
	string strDirD = convertDMI.str();
	string dirAddressD = "conf/D-" + strDirD;
	char *dirNameD = new char[dirAddressD.length() + 1];
	strcpy(dirNameD, dirAddressD.c_str());
	int resultD = mkdir(dirNameD, 0775);
	//*/

	ostringstream convert;
	convert << Name;
	string strName = convert.str();
	//	string address = dirAddressD + "/sys-" + strName + ".txt";
	string address = "conf/" + filename + "-" + strName + ".txt";
	ofstream conf(address.c_str()); //, ios_base::app);						   //, ios_base::app);

	for (int k = 0; k < length; ++k)
	{
		for (int i = 0; i < width; ++i)
		{
			for (int j = 0; j < height; ++j)
			{
				conf << fixed << i << "\t" << j << "\t" << k << "\t"
					 << spin[k * width * height + i * height + j].X << "\t" << spin[k * width * height + i * height + j].Y << "\t" << spin[k * width * height + i * height + j].Z
					 << "\t" << dirJ << "\t" << dirD << "\t" << dirP << "\t" << dirA << "\t" << dirT
					 << "\t" << spin[k * width * height + i * height + j].Anisotropy
					 << "\t" << H_xy[k * width * height + i * height + j] 
					 << endl;
			}
		}
	}
	conf.close();
}


/**************************************************************************************************************
************************************************* *** *    *   * ** *******************************************
*************************************************  *  * ** ** **  * *******************************************
************************************************* * * *    ** ** *  *******************************************
************************************************* *** * ** ** ** ** *******************************************
************************************************* *** * ** *   * ** *******************************************
**************************************************************************************************************/
// Основная программа

int main(int argc, char **argv)
{
	cout << "\n\tSTART\n" << endl;

	// "инициализация" генератора случайных чисел
	srand(time(NULL)); //	srand((unsigned)time(NULL)); // для рандмного спина

	int width = WIDTH, height = HEIGHT, length = LENGTH;
	int n = N;
	unsigned int MK_STEP = STEP;
	unsigned int elt = ELT;

	Spin_sys(spin, 1., 0.); //Заполнение системы ,1.); - зеленая || ,1.,0.); - красная\\,acos(1./4.); // // M_PI_4, M_PI_2); - Оптимально
	neighbor(spin);

	int ConfNum = 0; // для визуализации

	//	Anis(spin,0.);
	//	Anis_grad(spin, 1.);
	/*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
						Градиент поля Y
	&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&*/	
	double H_xy_min = 0.0, H_xy_max = 0.225;
	int H_xy_part = 5; // количество лесенок с разным значением поля  min = 2 | max = width/2.


	double step_H_xy = (H_xy_max-H_xy_min) *  1./double(H_xy_part-1);//(width/2.);
	int i_start = 12;
	double H_xy_val = H_xy_min;
	double H_xy[N] = {0.};

	/*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&*/


	config(ConfNum++, 0, 0, 0, 0, 0, H_xy, spin);
	//cout << "\n\tOK" << endl; if (_getch() == 27) exit(0);

	double Jmin = Jex;
	double Jmax = maxJ;
	double Jstep = stepJ;

	double Dmin = DMI;
	double Dmax = maxD;
	double Dstep = stepD;

	double Pmin = POLE;
	double Pmax = maxP;
	double Pstep = stepP;

	double Amin = An;
	double Amax = maxA;
	double Astep = stepA;

	int Tmin = minT;
	int Tmax = maxT;
	int Tstep = stepT;

	int cycle_num = CYCLE;

	int Ji = (float)(Jmax - Jmin) / Jstep + 1.;
	int Di = (float)(Dmax - Dmin) / Dstep + 1.;
	int Pi = (float)(Pmax - Pmin) / Pstep + 1.;
	int Ai = (float)(Amax - Amin) / Astep + 1.;
	int Ti = ceil(((float)Tmax - (float)Tmin) / (float)Tstep);
	cout << "\tJi = " << Ji << "\tDi = " << Di << "\tPi = " << Pi << "\tAi = " << Ai << "\tTi = " << Ti << "\tMi = " << MK_STEP << endl;
	unsigned long long int max_iteration = (unsigned long long int)Ji * Di * Pi * Ai * Ti * MK_STEP * cycle_num; //большие числа - лучше всегда объявлять тип данных для обоих половин выражений
																												 //	cout << "max_iteration = " << max_iteration << endl;
	unsigned long long int number_of_files = (unsigned long long int)max_iteration / elt;
	cout << "number_of_files = " << number_of_files << "\n" << endl;

	unsigned long long int status = 0;
	int status_10_percent = (max_iteration) / 10., status_num = 0;
	unsigned long long int iteration = 0;

	double average_energy = 0.;
	

	for (double jex = 0, J = Jmin; jex < Ji; J += Jstep, ++jex)
	{
		for (double a = 0, A = Amin; a < Ai; A += Astep, ++a)
		{
			//	Anis(spin, A);
		 //	Anis_grad(spin, 1., -1.);

			for (double d = 0, D = Dmin; d < Di; D += Dstep, ++d)
			{
			//	Spin_sys(spin, 1.); 
			//	 Spin_sys(spin);
				 
				for (double p = 0, P = Pmin; p < Pi; P += Pstep, ++p)
				{
					for (int TT = Tmin; TT < Tmax; TT += Tstep)
					{
						double T = (double)TT / _T; //1000.

						//sigma_model(spin, 16, 8, 2); // Создаем скирмион..ground_state(spin,0,0, J, D, P, A);
						//ground_state(spin,1,0, J, D, P, A);
						sigma_model(spin, 12, 8, 2, 1);
						config(ConfNum++, J, D, P, A, T,H_xy, spin);
						int imp_x = 1;
						int pulse = 1;
						int imp_time = 0;

						for (int cycle = 0; cycle < cycle_num; ++cycle)
						{
//							Spin_sys(spin, 1., 0.);
//						//	int imp_x = cycle+1;
//							
//							sigma_model(spin, 16, 8, 2); // Создаем скирмион
//							sigma_model(spin, 32, 8, 2);
//							sigma_model(spin, 48, 8, 2);
							
						//	int pA=0;
						
							

							for (int MK = 0; MK < MK_STEP; ++MK)
							{
								// int num = rand() % n;
								// monte_carlo(spin, num, T, J, P, D, A);
								
								/*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&*/
								//////////////////   движение градиента   //////////////////

								 // длительность задается через CYCLE и STEP

								 if (MK == 0 or (MK + 1) % int(STEP * 1.0) == 0) //(чем меньше число STEP * 1.0 тем быстрее движется)
								 {	
								 	for (int i = 0; i < width; ++i)
								 	{	
								 		int i_new = (i_start+i)%(width);
								 		for (int j = 0; j < height; ++j)
								 		{
											
								 			H_xy[i_new * height + j] = H_xy_val;
								 		}
										
								 			if ((i+1) % ((width/2)/(H_xy_part-1)) == 0) // смена лесенкой через part 
								 			{
								 				if(i<width/2)
								 					H_xy_val += step_H_xy;
								 				else
								 					H_xy_val -= step_H_xy;
											}
											
								 	}
									
								 	i_start += 1;
								 	if (i_start > width ) i_start = 0;
								 }
								
								int num = rand() % n;
								monte_carlo(spin, num, T, J, P, D, A, H_xy[num], H_xy[num]); // По XY
								//monte_carlo(spin, num, T, J, P, D, A, H_xy[num]); // По X
								//monte_carlo(spin, num, T, J, P, D, A, 0.0, H_xy[num]); // По Y
								
								/*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&*/
								
								
							//////?????????????????????????????????????????????????????????????????????????Импульс	
//								double POLE_XY = 0.;		
//								int ri = rand()%(width);
//								int rj = rand()%(height);
//								int num = ri * height + rj;										
																			
//								if ((imp_x <= ri && ri < imp_x+25) && (10 <= rj && rj < 25)){ // область действия импульса
//									if (pulse) POLE_XY = 10;
//									else
//										POLE_XY = 0.;
//								}									
//									
//								if (++imp_time == 2e6){// длительность импульса
//									imp_time=0;
//									pulse = !pulse;
//								}

							//	STT = 0.1;				
								//monte_carlo(spin, num, T, J, P, D, A,POLE_XY,POLE_XY);

//								if ((MK + 1) % int(STEP * 0.5) == 0){// скорость движения импульса (чем меньше число тем быстрее) | длительность моделирования можно уселичить CYCLE
//									imp_x += 1;
//									if (imp_x > width ) imp_x = 0;
//								}
							//?????????????????????????????????????????????????????????????????????????*/
								
								//__________________________________________ВИЗУАЛИЗАЦИЯ-MK__________________________________________
								if ((MK + 1) % elt == 0)
								{
									config(ConfNum++, J, D, P, A, T, H_xy, spin);
								}
								//___________________________________________________________________________________________________*/
								
						
						/*/?????????????????????????????????????????????????????????????????????????Градиент анизотропии
								if ((MK) % 10000000 == 0 && pA < width)
								{ // Начиная с 1000000 МК шага создаем вертикальные полосы анизотропии
									for (int gj = 0; gj < HEIGHT; ++gj)
										spin[pA * height + gj].Anisotropy = 10.;
									++pA;
									
									config(ConfNum++, J, D, P, A, T, spin);												
								}
						//*/


								
								if ((status <= iteration++) && (status_num <= 100))
								{
									cout << fixed << "Status: " << status_num++ * 10 << " %" << endl; //* (float)(iteration) / (float)(max_iteration)
									//cout.flush();
									status += status_10_percent; //(int)
									--status;
									if (status_num > 100)
									{
										cout << "\n\tStatus error\n" << endl;
									}
								}
							}

							//average_energy += Efull(spin);
								for (int ae = 0; ae < N; ++ae)
								{
									average_energy += Esp(spin, ae, J, P, D, A, H_xy[ae], H_xy[ae]);
								}
								average_energy /= 2.*N;
						}
					}
				}
			}
		}
	}
	//Exit:
	//delete[] spin; //Чистим память (Динамическую)

	cout <<" E = " << average_energy/cycle_num << "\t Hex = " << Hex(spin) << endl;

	cout << "\n\tSTOP" << endl;

	return 0;
}
